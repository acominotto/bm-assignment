declare module 'redux-map-gl' {
    import { AnyAction, ActionCreator }       from 'redux';
    export const onChangeViewport: (vp: {
        latitude: number,
        longitude: number,
        zoom: number,
        bearing: number,
        pitch: number,
        width: number,
        height: number,
    }) => AnyAction
    export const createViewportReducer: () => (state: any, action: AnyAction) => any
    export default function(red: (state: any, action: AnyAction) => any, vp: any, overrideName?: string): (state: any, action: AnyAction) => any
}