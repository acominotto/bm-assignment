import { AnyAction }                            from 'redux';
import { Vehicle }                              from '../types';
import { BuildAction, ActionBuilder }           from 'utils';

export const UpdateFleet: ActionBuilder < void > = BuildAction< void >("UpdateFleet");

export const FleetUpdated: ActionBuilder < Vehicle[] > = BuildAction< Vehicle[] >("FleetUpdated");

export const FilterFleet: ActionBuilder < number[] > = BuildAction< number[] >("FilterFleet");

export const SelectVehicle: ActionBuilder < string > = BuildAction< string >("SelectVehicle");

export const UnSelectVehicle: ActionBuilder < void > = BuildAction< string >("UnSelectVehicle");

export const MoveSelected: ActionBuilder< Vehicle[] > = BuildAction< string >("MoveSelected");