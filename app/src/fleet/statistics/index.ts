
import { Vehicle }                          from '../types';
import * as constants                       from '../constants';

export type Statistics = {
    occupancy: number;
    totalOccupancy: number;
    total: number;
    totalInactive: number;
    totalIdle: number;
    totalActive: number;
    totalError: number;
}

function incrementForStatus(status: number, vehicle: Vehicle) {
    return vehicle.status.id === status ? 1 : 0
}

export function getStats(fleet: Vehicle[]): Statistics {
    return fleet.reduce((acc, v) => {
        const {occupancy, totalOccupancy, total, totalInactive, totalActive, totalError, totalIdle} = acc;
        return {
            occupancy: occupancy + v.occupancy,
            totalOccupancy: totalOccupancy + v.max_occupancy,
            total: total + 1,
            totalInactive: totalInactive + incrementForStatus(constants.INACTIVE, v),
            totalIdle: totalIdle + incrementForStatus(constants.IDLE, v),
            totalActive: totalActive + incrementForStatus(constants.ACTIVE, v),
            totalError: totalError  + incrementForStatus(constants.ERROR, v)
        }
    }, {
        occupancy: 0,
        totalOccupancy: 0,
        total: 0,
        totalInactive: 0,
        totalIdle: 0,
        totalActive: 0,
        totalError: 0
    });
} 