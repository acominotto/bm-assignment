export type Position = [number, number]

export type Status = {
    id: number,
    msg: string
}

export type Vehicle = {
    name  : string,
    id    : string,
    status: Status, 
    position: Position,
    bearing: number,
    speed: number,
    occupancy: number,
    max_occupancy: number
}