
import { Epic }                         from 'redux-observable';
import { Observable }                   from 'rxjs';
import { AnyAction }                    from 'redux';
import * as actions                     from '../actions';
import { Vehicle }                      from '../types';



function loadFleet(): Observable< Vehicle[] > {
    return Observable.ajax.getJSON('/api/fleet.json')
} 

const updateFleet: Epic< AnyAction, any > = ($action => {
    return $action.ofType( actions.UpdateFleet.Type ) 
        .flatMap(( a: AnyAction ) => {
            return Observable.interval(1000).flatMap( _ => 
                loadFleet()
                .flatMap(fleet => [actions.MoveSelected(fleet), actions.FleetUpdated(fleet)])
            )
        });
});

export default updateFleet;