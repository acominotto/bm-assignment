import { Statistics, getStats }              from './statistics';
import { Vehicle }                           from './types';
import * as assert                           from 'power-assert'

describe('statistics', () => {
    it('should return empty stats for empty vehicles', () => {
        assert.deepEqual(getStats([]), {
            occupancy: 0,
            totalOccupancy: 0,
            total: 0,
            totalInactive: 0,
            totalIdle: 0,
            totalActive: 0,
            totalError: 0
        });
    });
    it('should return stats for one vehicle', () =>{
        const fleet: Vehicle[] = [{"name":"VHC-0","id":"453bfe84-f5f2-4b3a-a542-99f52d9edf11","status":{"id":2,"msg":""},"position":[-122.47117621962494,37.747421856360816],"bearing":-584.6048698426108,"speed":16.666666666666668,"occupancy":0,"max_occupancy":4}]
        assert.deepEqual(getStats(fleet), {
            occupancy: 0,
            totalOccupancy: 4,
            total: 1,
            totalInactive: 0,
            totalIdle: 0,
            totalActive: 1,
            totalError: 0
        })
    })
    it('should return stats for five vehicles', () =>{
        const fleet: Vehicle[] = [{"name":"VHC-0","id":"453bfe84-f5f2-4b3a-a542-99f52d9edf11","status":{"id":2,"msg":""},"position":[-122.47117621962494,37.747421856360816],"bearing":-584.6048698426108,"speed":16.666666666666668,"occupancy":0,"max_occupancy":4},{"name":"VHC-1","id":"c71b93ba-2ec0-45e0-83da-6c4d23f762ab","status":{"id":2,"msg":""},"position":[-122.62861310993434,37.73473256838363],"bearing":67.0554945268085,"speed":16.666666666666668,"occupancy":2,"max_occupancy":5},{"name":"VHC-2","id":"94e7ffc0-1513-48b7-9bf9-33e290bb365a","status":{"id":0,"msg":""},"position":[-122.45461159037215,37.72088197692426],"bearing":1547.2802175251422,"speed":0,"occupancy":0,"max_occupancy":3},{"name":"VHC-3","id":"0aa2edf0-dc15-4af3-a5d0-94609d705dc1","status":{"id":3,"msg":"Random error with code 5000"},"position":[-122.20652771964036,37.77263165270663],"bearing":934.9001001778169,"speed":16.666666666666668,"occupancy":0,"max_occupancy":3},{"name":"VHC-4","id":"1602a714-cea4-476f-a8c5-6b99c00ff2a9","status":{"id":0,"msg":""},"position":[-122.50038406276477,37.72224627008284],"bearing":333.41830304826374,"speed":0,"occupancy":0,"max_occupancy":1}];
        assert.deepEqual(getStats(fleet), {
            occupancy: 2,
            totalOccupancy: 16,
            total: 5,
            totalInactive: 2,
            totalIdle: 0,
            totalActive: 2,
            totalError: 1
        })
    })
});