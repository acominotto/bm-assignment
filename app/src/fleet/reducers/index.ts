
import { Reducer, AnyAction, combineReducers } from 'redux';
import * as actions from '../actions';
import { Vehicle } from '../types'


export default (state: {vehicles: Vehicle[], selected?: Vehicle, filters: number[]} = { vehicles: [], filters: [] }, action: AnyAction) => {
    switch (action.type) {
        case actions.FleetUpdated.Type: return {...state, vehicles: action.data};
        case actions.SelectVehicle.Type: return {...state, selected: state.vehicles.find(_ => _.id === action.data)}
        case actions.UnSelectVehicle.Type: return {...state, selected: undefined}
        case actions.MoveSelected.Type: 
            if(!state.selected) return state;
            return {...state, 
                selected: action.data.find(_ => _.id === (state.selected as Vehicle).id)
            }
        case actions.FilterFleet.Type: return {...state, filters: action.data, };
        default: return state;
    }
}

