import * as React from 'react';
import { Vehicle, constants } from 'fleet';
import * as classNames                                                       from 'classnames';
import './recap_vehicle.scss';

const StatusMap = ['INACTIVE', 'IDLE', 'ACTIVE', 'ERROR'];


const VehicleRecapGroup = ({label, value, className}: {label: string, value: string, className?: string}) => (
    <div className={classNames("vehicle-recap-group", className)} >
        <span className="vehicle-recap-group-label">{label}</span>
        <span className="vehicle-recap-group-value">{value}</span>
    </div>
)

export type Props = { vehicle?: Vehicle}

export const VehicleRecap: React.SFC< Props > = ({ vehicle }) => {
    if(!vehicle) return null;
    const inError = vehicle.status.id === constants.ERROR;
    return (
        <div className="vehicle-recap">
            <span className="vehicle-recap-name">{`${vehicle.name} (${StatusMap[vehicle.status.id]})`}</span>
            <VehicleRecapGroup label="id" value={vehicle.id} />
            <VehicleRecapGroup label="speed" value={`${Math.round(vehicle.speed)} km/h`} />
            <VehicleRecapGroup label="long" value={`${vehicle.position[0]}`} />
            <VehicleRecapGroup label="lat" value={`${vehicle.position[1]}`} />
            <VehicleRecapGroup label="pass" value={`${vehicle.occupancy} / ${vehicle.max_occupancy}`} />
            { inError && <VehicleRecapGroup label="err" value={vehicle.status.msg} /> }
        </div>
    );
}