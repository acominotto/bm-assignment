import * as React                               from 'react';
import { Dropdown, DropdownItemProps }          from 'semantic-ui-react';
import { Vehicle }                              from 'fleet';


import './select_vehicle.scss';

export type Props = {
    options: Vehicle[],
    onSelect: (vId: string) => void
}

function vehicleToOption(v: Vehicle): DropdownItemProps {
    return { key: v.id, text: v.name, value: v.id };
}

export const Select: React.SFC< Props > = ({ options, onSelect}) => (
    <Dropdown 
        placeholder='Select Vehicle...'
        className="select-vehicle"
        search 
        selection
        options={options.map(vehicleToOption)} 
        onChange={ (e, v)=> { onSelect(v.value as string)} } />
);
