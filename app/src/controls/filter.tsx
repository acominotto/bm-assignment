import * as React                               from 'react';
import { Dropdown, DropdownItemProps }          from 'semantic-ui-react';
import './filter.scss';

const options: DropdownItemProps[] = [
    {key: 'inactive', text: 'Inactive', value: 0},
    {key: 'idle', text: 'Idle', value: 1},
    {key: 'active', text: 'Active', value: 2},
    {key: 'error', text: 'Error', value: 3}
]
export type Props = {
    onChangeFilter: (fs: number[]) => void;
}

export const StatusFilter: React.SFC< Props > = (props) => (
    <Dropdown className="status-filter" placeholder='Filter' multiple selection options={options} onChange={(e, data) => {debugger; props.onChangeFilter(data.value as number[])}} />
);