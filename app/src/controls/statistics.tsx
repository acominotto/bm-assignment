import * as React                   from 'react';
import { Statistics }               from 'fleet';
import { percentage }               from 'utils';

import './statistics.scss';

export type StatProps = {
    statistics: Statistics
}


const StatisticsCard: React.SFC< StatProps > = ({ statistics }) => (
    <div className="statistics-card">
        <h1>Stats</h1>
        <div className="statistics-card-group">
            <span className="statistics-card-group-label">Passengers</span>
            <span className="">{`${statistics.occupancy} / ${statistics.totalOccupancy} (${percentage(statistics.occupancy, statistics.totalOccupancy)}%)`}</span>
        </div>
        <div className="statistics-card-group">
            <span className="statistics-card-group-label">Inactive</span>
            <span className="">{`${statistics.totalInactive} (${percentage(statistics.totalInactive, statistics.total)}%)`}</span>
        </div>
        <div className="statistics-card-group">
            <span className="statistics-card-group-label">Idle</span>
            <span className="">{`${statistics.totalIdle} (${percentage(statistics.totalIdle, statistics.total)}%)`}</span>
        </div>
        <div className="statistics-card-group">
            <span className="statistics-card-group-label">Active</span>
            <span className="">{`${statistics.totalActive} (${percentage(statistics.totalActive, statistics.total)}%)`}</span>
        </div>
        <div className="statistics-card-group">
            <span className="statistics-card-group-label">Error</span>
            <span className="">{`${statistics.totalError} (${percentage(statistics.totalError, statistics.total)}%)`}</span>
        </div>
        <div className="statistics-card-group statistics-card-group-total">
            <span className="statistics-card-group-label">total</span>
            <span className="">{`${statistics.total} (100%)`}</span>
        </div>
    </div>
);
export default StatisticsCard;