import * as React                                       from 'react';

import { Button, Icon }                                 from 'semantic-ui-react';
import { Select, Props as SVProps }                     from './select_vehicle';
import { VehicleRecap, Props as VRProps }               from './recap_vehicle';
import { StatusFilter, Props as FProps }                from './filter';
import Statistics, { StatProps }                       from './statistics';
import './index.scss';

export type Props = SVProps & VRProps & FProps & StatProps & {
    resetTrips: () => void
}

const Controls: React.SFC< Props > = (props) => (
    <div className="map-overlay">
        <div className="map-headers">
            <div className="map-controls">
                <div>
                    <Button primary onClick={ () => props.resetTrips() }><Icon name="refresh" /></Button>
                    <Select options={props.options} onSelect={e => { props.onSelect(e)}  }/>
                </div>
                <div>
                    <StatusFilter onChangeFilter={e => { props.onChangeFilter(e)}  }/>
                </div>
            </div>
            <VehicleRecap vehicle={ props.vehicle } />
        </div>
        
            
        <div className="map-stats">
            <Statistics statistics={props.statistics} />
        </div>
    </div>
)

export default Controls;