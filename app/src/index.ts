
import './index.scss';

import * as React from  'react';
import load  from './app';

declare global {
    interface Window { React: any, MapboxAccessToken: string}
}

function startApp() {
    window.React = React;
    load();
}


startApp();