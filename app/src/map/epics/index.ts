
import { Epic }                         from 'redux-observable';
import { Observable }                   from 'rxjs';
import { AnyAction }                    from 'redux';
import * as actions                     from 'fleet/actions';
import { Vehicle }                      from 'fleet/types';
import {onChangeViewport as onViewportChange }                               from 'redux-map-gl';


function loadFleet(): Observable< Vehicle[] > {
    return Observable.ajax.getJSON('/api/fleet.json')
} 

const updateViewPort: Epic< AnyAction, any > = (($action, state) => {
    return $action.ofType( actions.SelectVehicle.Type ) 
        .map(( a: AnyAction ) => {
            const gState = state.getState()
            const selected = gState.fleet.selected as Vehicle;
            return onViewportChange({...gState.map.viewport, longitude: selected.position[0], latitude: selected.position[1], zoom: 16 });
        });
});
export default updateViewPort;