import { Vehicle } from 'fleet';

import { Feature, LineString } from 'geojson';

export function toGeo(v: Vehicle): Feature< LineString > {
    return {
        type: "Feature",
        geometry: {
            type: "LineString",
            coordinates: [v.position]
        },
        properties: {
            vehicle: v
        }
    }
}

