
import { Feature,  FeatureCollection, GeometryObject } from 'geojson';

import * as vehicle from './vehicle_functions';

export function featureCollection< G extends GeometryObject >(features: Feature< G >[]): FeatureCollection< G > {
    return {
        type: "FeatureCollection",
        features
    }
}

export { vehicle }