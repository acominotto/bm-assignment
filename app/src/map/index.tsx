import {connect, Dispatch}                                                             from 'react-redux';
import {onChangeViewport as onViewportChange }                               from 'redux-map-gl';

import { Map as MapComp, ViewPort, Actions as MapActions, Props as MapProps } from './components/map';
export *                                                                     from './map_style'
import reducers                                                              from './reducers';
import {fromJS}                                                              from 'immutable';
import { Vehicle, actions, getStats }                                                  from 'fleet';
import epics                                                                 from './epics';
import { ResetMap }                                                          from './actions';



function mapStateToProps(state, ownProps: {width?: number, height?: number}): MapProps {
  const mapState = state.map.viewport.toJS() as ViewPort;
  const mapStyle = state.map.style;
  const fleet = state.fleet.filters.length > 0 ? state.fleet.vehicles.filter(v => state.fleet.filters.indexOf(v.status.id) > -1) : state.fleet.vehicles;

  const selected = state.fleet.selected as Vehicle;
  const statistics = getStats(state.fleet.vehicles)
  
  return {
    ...mapState,
    mapStyle,
    fleet,
    selectedVehicle: selected,
    statistics,
    ...ownProps
  };
}

const dispatches : MapActions = {
    onViewportChange,
    selectVehicle: actions.SelectVehicle,
    changeFilter: actions.FilterFleet,
    resetTrips: ResetMap
};

export const Map = connect(mapStateToProps, dispatches)(MapComp);

export { reducers, epics }