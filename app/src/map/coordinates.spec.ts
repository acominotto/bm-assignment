import { Vehicle }              from 'fleet';
import * as assert                           from 'power-assert'
import {vehicle}                  from './coordinates';

describe('vehicle function', () => {
    it('should transform to geo point', () => {
        const geo = vehicle.toGeo({"name":"VHC-0","id":"453bfe84-f5f2-4b3a-a542-99f52d9edf11","status":{"id":2,"msg":""},"position":[-122.47117621962494,37.747421856360816],"bearing":-584.6048698426108,"speed":16.666666666666668,"occupancy":0,"max_occupancy":4})
        assert.deepEqual(geo, {
              "geometry": {
                "coordinates": [
                 [
                    -122.47117621962494,
                    37.747421856360816
                  ]
                ],
                "type": "LineString"
              },
              "properties": {
                "vehicle": {
                  "bearing": -584.6048698426108,
                  "id": "453bfe84-f5f2-4b3a-a542-99f52d9edf11",
                  "max_occupancy": 4,
                  "name": "VHC-0",
                  "occupancy": 0,
                  "position": [
                    -122.47117621962494,
                    37.747421856360816
                  ],
                  "speed": 16.666666666666668,
                  "status": {
                    "id": 2,
                    "msg": ""
                  }
                }
              },
              "type": "Feature"
            });
    })
})