import { Reducer, AnyAction, combineReducers }  from 'redux';
import { MAP_STYLE }                            from '../map_style'

import enhanceMapReducer                        from 'redux-map-gl';
import { Vehicle, actions }                              from 'fleet';
import { ResetMap }                             from '../actions';
import { vehicle, featureCollection }                                        from '../coordinates';
import {fromJS}                                                              from 'immutable';


function mergeIntoMapStyle(ms: any, fleet: Vehicle[]): any {
    if(!ms.hasIn(['sources', 'fleet'])) {
        return ms.setIn(['sources', 'fleet'] , fromJS({type: "geojson", data: featureCollection(fleet.map( vehicle.toGeo ))}));
    } else {
        const toto = fleet.reduce((acc, v, i) => {
             return acc.setIn(['sources', 'fleet', 'data', 'features', i, 'geometry', 'coordinates'],
                acc.getIn(['sources', 'fleet', 'data', 'features', i, 'geometry', 'coordinates']).push(v.position));
        }, ms);
        return toto;
    }
}

function resetLineStrings(ms: any): any {
    const features = ms.getIn(['sources', 'fleet', 'data', 'features'])
        .map(e => {
            const arr = e.getIn(['geometry', 'coordinates'])
            return e.setIn(['geometry', 'coordinates'], fromJS([arr.get(arr.size - 1)]))
        })
    return ms.setIn(['sources', 'fleet', 'data', 'features'], features);
}

export const StyleReducer = (state: any, action: AnyAction) => {
    switch(action.type) {
        case actions.FleetUpdated.Type: return {...state, style: mergeIntoMapStyle(state.style || MAP_STYLE, action.data) }
        case ResetMap.Type: return {...state, style: resetLineStrings(state.style)};
        default: return state;
    }
}

const initialValue = {
    latitude: 37.785164,
    longitude: -122.41669,
    zoom: 10,
    bearing: 0,
    pitch: 0
}
export default enhanceMapReducer(StyleReducer, initialValue)