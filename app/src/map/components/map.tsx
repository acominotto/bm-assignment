
import MapGL, { Marker, Popup }                                              from 'react-map-gl';
import * as classNames                                                       from 'classnames';
import * as React                                                            from 'react'
import {createViewportReducer}                                               from 'redux-map-gl';
import { Vehicle, constants }                                                           from 'fleet';
import { vehicle, featureCollection }                                        from '../coordinates';
import { fleetLayer }                                                        from '../map_style';
import {fromJS}                                                              from 'immutable'        
import Pin                                                                   from './pin_vehicle';
import Controls                                                              from 'controls';
import { Statistics }                                                        from 'fleet';
import './map.scss';

const MapboxAccessToken = "pk.eyJ1IjoiYWNvbWlub3R0byIsImEiOiJjajh6bThrNjcwZDcyMnZwZWtpbW50b2F2In0.N0Wi0UM5L2nWMRh0ioqGgw"



export type ViewPort = {
    latitude: number,
    longitude: number,
    zoom: number,
    bearing: number,
    pitch: number,
}

export type Actions = {
    onViewportChange: (vp: ViewPort) => void,
    selectVehicle: (vId: string) => void,
    changeFilter: (fs: number[]) => void,
    resetTrips: () => void
}

export type Props = ViewPort & {
    
    width?: number,
    height?: number,
    mapStyle: any,
    fleet: Vehicle[],
    selectedVehicle?: Vehicle,
    statistics: Statistics
}

export type State = {
    popVehicle?: Vehicle
}
const VehiclePopup: React.SFC< { vehicle?: Vehicle, onClose: Function } >  = ({ vehicle, onClose }) => {
    if(!vehicle) return null;
    const inError = vehicle.status.id === constants.ERROR
    return (
        <Popup
            anchor="top"
            longitude={vehicle.position[0]} 
            latitude={vehicle.position[1]}
            onClose={ onClose }
        >
            <div className={classNames("vehicle-popup", {'vehicle-popup--error': inError})}>
                <span>{vehicle.name}</span>
                <span>{Math.round(vehicle.speed)} km/h</span>
                { inError && <span>{ vehicle.status.msg }</span>  }
            </div>
        </Popup>
    )
}




function colorOfVehicle(v: Vehicle) {
    switch(v.status.id) {
        case constants.INACTIVE: return 'azure';
        case constants.IDLE: return 'orange';
        case constants.ACTIVE: return 'green';
        case constants.ERROR: return 'red';
    }
}


export class Map extends React.Component< Props & Actions, State > {
    state: State = {};


    render() {
        const props = this.props;
        
        if(props.fleet.length === 0)
            return <h1>loading...</h1>;
        return (
            <MapGL
                {...props}
                mapboxApiAccessToken={ MapboxAccessToken }
            >
                <Controls 
                    resetTrips={ () => props.resetTrips() }
                    vehicle={props.selectedVehicle} 
                    onSelect={ e => props.selectVehicle(e) } 
                    options={props.fleet} 
                    onChangeFilter={fs => props.changeFilter(fs)} 
                    statistics={ props.statistics }
                />
                { props.fleet.map(v =>
                    <Marker key={`marker-${v.id}`} longitude={v.position[0]} latitude={v.position[1]}>
                        <Pin size={20} color={colorOfVehicle(v)}  onClick={(e: any) => this.setState({popVehicle: v})}/>
                    </Marker>
                ) }

                
                <VehiclePopup vehicle={ this.state.popVehicle } onClose={() => this.setState({popVehicle: undefined})} />
            </MapGL>
        )
    }
}
