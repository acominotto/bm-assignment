import { AnyAction }                            from 'redux';
import { BuildAction, ActionBuilder }           from 'utils';

export const ResetMap: ActionBuilder < void > = BuildAction< void >("ResetMap");