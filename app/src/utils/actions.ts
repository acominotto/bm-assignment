import {AnyAction, ActionCreator} from 'redux';

export type ActionBuilder< D = any > = ((data: D) => AnyAction) & { Type: string } & ActionCreator< AnyAction >;

export function BuildAction< D > (type: string): ActionBuilder< D >{
    const fn: any = (data: D) => ({ type, data })
    fn.Type = type;
    return fn as ActionBuilder< D >;
}