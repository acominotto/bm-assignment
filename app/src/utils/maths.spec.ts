import { percentage }                       from './maths';
import * as assert                           from 'power-assert'

describe('percentage', () => {
    it('should not divide by 0', () => {
        assert.equal(percentage(0, 0), 0);
    });
    it('should fail if total < count', () => {
        assert.throws( () => percentage(100, 10));
    });
    it('should return 100 if input == total', () =>{
        assert.equal(percentage(42, 42), 100);
    })
    it('should return the percentage in normal conditions', () =>{
        assert.equal(percentage(42, 126), 33);
    })
});