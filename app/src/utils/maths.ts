export function percentage(count: number, total: number): number {
    if(total < count) throw new Error(`invalid percentage: total:${total} < count:${count} `)
    if(total === 0) return 0;
    return Math.round( count / total * 100 );

}