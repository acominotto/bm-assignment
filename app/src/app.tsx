// @formatter:off

import 'rxjs/Rx';
import 'reflect-metadata'
import * as React                                                            from 'react'
import * as ReactDom                                                         from 'react-dom'
import { Redirect, Router, withRouter, RouteComponentProps, Switch }         from 'react-router'
import createHistory                                                         from 'history/createHashHistory'
import { createStore, combineReducers, applyMiddleware }                     from 'redux'
import { composeWithDevTools }                                               from 'redux-devtools-extension'
import { createEpicMiddleware, combineEpics }                                from 'redux-observable'
import { ConnectedRouter, routerMiddleware, routerReducer }                  from 'react-router-redux'
import { Map, MAP_STYLE, reducers as MapReducers, epics as MapEpics }                           from 'map';
import { reducers as FleetReducers, epics as FleetEpics, actions }           from 'fleet';

import { Provider }                                                          from 'react-redux'
import { Input }                                                             from 'semantic-ui-react'



// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();


// Build the middleware for intercepting and dispatching navigation actions
const epicMiddleWare = createEpicMiddleware( combineEpics( FleetEpics, MapEpics ) );
const middleware = routerMiddleware( history );

let store = createStore( 
    combineReducers({ 
        map: MapReducers,
        fleet: FleetReducers,
    }),
    applyMiddleware(epicMiddleWare, middleware)
)
setTimeout( () => store.dispatch(actions.UpdateFleet(void 0)), 1000)

console.log(store)

const AppPage = () => (
    <Provider store={store}>
        <div className="map-wrapper">
            <Map width={window.innerWidth} height={window.innerHeight}>
            </Map>
        </div>
    </Provider>
);

export default function () {
    const app = document.getElementById( 'app' );
    if( app != null ) {
        ReactDom.render( <AppPage/>, app );
    }
}
