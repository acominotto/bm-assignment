
import * as pubsub      from 'pubsub-js'
import startFleetFn, 
{ Vehicle }             from './vehicles/vhcGenerator'   // nb of vehicles in the fleet
import * as express     from 'express';

const app = express();

const port = 3000

let fleet: Vehicle[] = [];

app.get('/fleet.json', (_, res) => res.status(200).json(fleet))

app.get( '/*', function ( req, res ) {
  res.status( 404 ).json({
    code: 404,
    error: 'Not Found',
    message: 'No matching API route',
  });
});

app.listen( port, function () {
  console.log( `Listening on port ${port}...` )
});


const startFleet = startFleetFn(500)

/**
 * subscribe to pubsub event to receive vehicle status
 * topic: name of the topic,
 * vehicles: array of vehicle status message
 *
 * Feel free to move/encapsulate this function where you want
 */
pubsub.subscribe('vehicles-status', (topic: any, vehicles: Vehicle[]) => {
  // TODO: 'vehicles' contains data for all vehicles in the fleet
  fleet =  vehicles;
})

/**
 * Starts generating messages from each vehicles in the fleet :
 * Each second, status for all vehicles are published through pubsub
 * in the topic 'vehicles-status'
 * DO NOT REMOVE THIS LINE (or you'll get disconnected form the backend)
 */
startFleet()
console.log('Fleet operations started')