
const INACTIVE = 0, IDLE = 1, ACTIVE = 2, ERROR = 3

const error = () => 'Random error with code ' + Math.ceil((Math.random() * 9)) * 1000

export type Status = {
  id: number,
  msg: string
}

export const randomStatus: () => Status = () => {
  const status = Math.ceil(Math.random() * 4) - 1
  return {
    id: status,
    msg: status === ERROR ? error() : ''
  }
}

export const randomOccupancy = (currentStatus: Status, nextStatus: Status, max: number, previousOccupancy: number) => {
  switch(nextStatus.id) {
    case ACTIVE:
    case ERROR:
      return currentStatus.id <= 1 && Math.random() > 0.5 ? Math.ceil(Math.random() * max) - 1 : previousOccupancy
    default: return 0
  }
}