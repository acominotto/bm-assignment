export declare type Status = {
    id: number;
    msg: string;
};
export declare const randomStatus: () => Status;
export declare const randomOccupancy: (currentStatus: Status, nextStatus: Status, max: number, previousOccupancy: number) => number;
