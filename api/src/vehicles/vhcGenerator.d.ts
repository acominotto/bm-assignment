/// <reference types="node" />
import { Position } from './positions';
import { Status } from './status';
export declare type Vehicle = {
    name: string;
    id: string;
    status: Status;
    position: Position;
    bearing: number;
    speed: number;
    occupancy: number;
    max_occupancy: number;
};
declare const _default: (nbVhc: number) => () => NodeJS.Timer;
export default _default;
