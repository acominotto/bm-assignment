/**
 * HELPER FUNCTIONS FOR THE FLEET GENERATOR.
 *
 * DO NOT EDIT THIS FILE
 */
export const randomBearing = (bearing?: number) => bearing ? bearing + (Math.random() - 0.5) * 90 : Math.random() * 360 // if a bearing is passed it can vary by ± 90°, otherwise it's random
export const randomRadius = () => Math.random() * 30

export type Position = [number, number]

export const nextPosition = ([lng, lat]: Position, bearing: number, distanceKm: number): Position => {
  const R = 6371; // Earth Radius in Km
  const lat2 = Math.asin(Math.sin(Math.PI / 180 * lat) * Math.cos(distanceKm / R) + Math.cos(Math.PI / 180 * lat) * Math.sin(distanceKm / R) * Math.cos(Math.PI / 180 * bearing));
  const lon2 = Math.PI / 180 * lng + Math.atan2(Math.sin( Math.PI / 180 * bearing) * Math.sin(distanceKm / R) * Math.cos( Math.PI / 180 * lat ), Math.cos(distanceKm / R) - Math.sin( Math.PI / 180 * lat) * Math.sin(lat2));
  return [180 / Math.PI * lon2, 180 / Math.PI * lat2]
}

export const randomPos = (lngLat: Position) => nextPosition(lngLat, randomBearing(), randomRadius())