"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pubsub = require("pubsub-js");
const vhcGenerator_1 = require("./vehicles/vhcGenerator"); // nb of vehicles in the fleet
const express = require("express");
const app = express();
const port = 3000;
let fleet = [];
app.get('/fleet.json', (_, res) => res.status(200).json(fleet));
app.get('/*', function (req, res) {
    res.status(404).json({
        code: 404,
        error: 'Not Found',
        message: 'No matching API route',
    });
});
app.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});
const startFleet = vhcGenerator_1.default(500);
/**
 * subscribe to pubsub event to receive vehicle status
 * topic: name of the topic,
 * vehicles: array of vehicle status message
 *
 * Feel free to move/encapsulate this function where you want
 */
pubsub.subscribe('vehicles-status', (topic, vehicles) => {
    // TODO: 'vehicles' contains data for all vehicles in the fleet
    fleet = vehicles;
});
/**
 * Starts generating messages from each vehicles in the fleet :
 * Each second, status for all vehicles are published through pubsub
 * in the topic 'vehicles-status'
 * DO NOT REMOVE THIS LINE (or you'll get disconnected form the backend)
 */
startFleet();
console.log('Fleet operations started');
//# sourceMappingURL=index.js.map